module gitlab.com/felipemocruha/kc-secret-decoder

go 1.12

require (
	github.com/logrusorgru/aurora v0.0.0-20190428105938-cea283e61946
	gopkg.in/yaml.v2 v2.2.2
	k8s.io/api v0.0.0-20190430012547-97d6bb8ea5f4
)
