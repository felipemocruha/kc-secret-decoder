package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"

	c "github.com/logrusorgru/aurora"
	v1 "k8s.io/api/core/v1"
)

func getData(resp []byte) (map[string][]byte, error) {
	s := &v1.Secret{}
	if err := json.Unmarshal(resp, s); err != nil {
		return nil, err
	}

	return s.Data, nil
}

func printSecretItem(k string, v []byte) {
	fmt.Printf("%v: \n%v\n", c.Bold(c.Yellow(k)), c.Cyan(string(v)))
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("No secret name provided.")
		os.Exit(1)
	}
	secretName := os.Args[1]

	out, err := exec.Command(
		"kubectl",
		"get",
		"secret",
		secretName,
		"-o",
		"json").Output()
	if err != nil {
		fmt.Errorf("Failed to get secret: %v", err)
		os.Exit(1)
	}

	data, err := getData(out)
	if err != nil {
		fmt.Printf("Failed to get secret data: %v", err)
		os.Exit(1)
	}

	for k, v := range data {
		printSecretItem(k, v)
	}
}
